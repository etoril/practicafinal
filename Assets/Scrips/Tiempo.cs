using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.PlayerLoop;

public class Tiempo : MonoBehaviour
{
    public static Tiempo instanciar;
    public Text Crono;
    private TimeSpan tiempoCrono;
    private bool timerBool;
    public float tiempoTrans;
    // Start is called before the first frame update

    public void Awake()
    {
        instanciar = this;
    }

    void Start()
    {
        Crono.text = "Tiempo: 00:00";
        timerBool = false;
    }

    public void iniciarTiempo()
    {
        timerBool = true;
        tiempoTrans =  300f;

        StartCoroutine(ActUpdate());
    }

    public void finTiempo()
    {
        timerBool = false;
    }

    private IEnumerator ActUpdate()
    {
        while (timerBool)
        {
             

            yield return null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        int minutos;
        float segundos;
        float decimas;
        tiempoTrans -= Time.deltaTime;
        tiempoCrono = TimeSpan.FromSeconds(tiempoTrans);
        minutos = (int)(tiempoTrans / 60);
        segundos = tiempoTrans % 60f;
        //           string tiempoCronoStr = "Tiempo: " + tiempoCrono.ToString("mm':'ss':'ff");
        Crono.text = minutos.ToString("00") + ":" + segundos.ToString("00");
    }
}
