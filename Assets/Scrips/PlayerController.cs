using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;
//using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{

	public float maxSpeed;
	public float speed;
	public bool tocasuelo;
	public GameObject player;

	private Rigidbody2D rb2d;
	private SpriteRenderer sr2d;
	private Animator anim;
	public float jumpPower;

	private bool jump;
	private bool disparar;
	private static bool muerto;

	public GameObject SonidoSalto;
	public GameObject SonidoCaida;
	public GameObject SonidoBola;
	

	public GameObject newSonidoSalto;
	public GameObject newSonidoCaida;
	public GameObject newSonidoBola;
	

	public Transform puntoDisparo;
	public GameObject bola;

	public Rigidbody2D cuerpoBola;
	public Rigidbody2D cuerpoDisparo;
	public GameObject newBola;

	public Vector2 direccion;

	public GameObject sonidoMatrEnemigo;
	public GameObject newsonidoMatrEnemigo;

	public GameObject sonidoTiempoAgotado;
	public GameObject newSonidoTiempoAgotado;

	private bool tiempoAgotado;


	Vector3 localscale;

	//// Use this for initialization
	void Start()
	{
		rb2d = player.GetComponent<Rigidbody2D>();
		sr2d = player.GetComponent<SpriteRenderer>();
		anim = player.GetComponent<Animator>();
		muerto = false;
		Tiempo.instanciar.iniciarTiempo();
		tiempoAgotado = false;
		
	}

	// Update is called once per frame
	void Update()
	{
		float h = Input.GetAxis("Horizontal");

		anim.SetFloat("speed", Mathf.Abs(rb2d.velocity.x));
		anim.SetBool("tocasuelo", tocasuelo);

		if (Input.GetKeyDown(KeyCode.UpArrow) && tocasuelo)
		{
			jump = true;
		}

		if(transform.position.y < -5.2)
        {
			Instantiate(SonidoCaida);
			SceneManager.LoadScene("FinPartida");
        }

		if (Input.GetKeyDown(KeyCode.Space))
        {
			disparar = true;
			
			newSonidoBola = Instantiate(SonidoBola);
			newBola = Instantiate(bola, puntoDisparo.position, Quaternion.identity);

			Destroy(newBola, 3);
			Destroy(newSonidoBola, 1);
		}

		if ( Tiempo.instanciar.tiempoTrans < 0 )
        {
			SceneManager.LoadScene("FinPartida");
		}

		if (Tiempo.instanciar.tiempoTrans < 60f && tiempoAgotado == false)
		{
			newSonidoTiempoAgotado = Instantiate(sonidoTiempoAgotado);
			tiempoAgotado = true;
		}

	}

	void FixedUpdate()
	{

		float h = Input.GetAxis("Horizontal");
		rb2d.AddForce(Vector2.right * speed * h);

		if (h > 0.1f)
		{
			rb2d.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

		}

		if (h < -0.1f)
		{
			rb2d.transform.localScale = new Vector3(-0.3f, 0.3f, 0.3f);

		}

		if (jump)
		{
			rb2d.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
			jump = false;
			tocasuelo = false;
			newSonidoSalto = Instantiate(SonidoSalto);
			Destroy(newSonidoSalto, 1);
		}
		if(disparar)
        {
			
		}
		Debug.Log(rb2d.velocity.x);
	}

	void OnCollisionStay2D(Collision2D col)
	{
		if (col.gameObject.tag == "suelo")
		{
			player.transform.parent = col.transform;
			tocasuelo = true;

		}

	}

	void OnCollisionExit2D(Collision2D col)
	{
		if (col.gameObject.tag != "suelo")
		{
			tocasuelo = false;
			player.transform.parent = null;
		}

	}

	private void OnTriggerExit2D(Collider2D collision)
	{

		if (collision.gameObject.tag == "Enemigo")
		{
			newsonidoMatrEnemigo = Instantiate(sonidoMatrEnemigo);
			Destroy(newsonidoMatrEnemigo, 1);
			Destroy(player);
			SceneManager.LoadScene("FinPartida");


		}


	}






}


