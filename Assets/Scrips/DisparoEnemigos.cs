using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoEnemigos : MonoBehaviour
{
    private Rigidbody2D bola;
    private Rigidbody2D newBola;
    public float Speed;
    public bool direccion;
    public GameObject matarEnemigo;
    public GameObject newMatarEnemigo;
    public GameObject enemigo;
    public GameObject newEnemigo;
    public Transform puntoDisparo;
    public GameObject bolaFuego;
    public GameObject newbolaFuego;


    // Start is called before the first frame update


    public void Start()
    {

        bola = GetComponent<Rigidbody2D>();
        if (enemigo.transform.localScale.x > 0)
        {
            bola.velocity = new Vector2((Speed), 0);
        }
        else
        {
            bola.velocity = new Vector2((-Speed), 0);
        }

    }

    // Update is called once per frame
    public void Update()
    {
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {
            newbolaFuego = bolaFuego;
            newMatarEnemigo = Instantiate(matarEnemigo);
            Destroy(newMatarEnemigo, 1);
            Destroy(newbolaFuego);
        }

    }

   
}
