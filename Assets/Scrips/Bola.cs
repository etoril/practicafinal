using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Bola : MonoBehaviour
{
    private Rigidbody2D bola;
    public float Speed;
    public bool direccion;
    public Transform posicionJugador;
    public GameObject matarEnemigo;
    public GameObject newmatarEnemigo;
    public GameObject bolaFuego;
    public GameObject newbolaFuego;
    public Transform playerTrans;
    public Transform enemyTrans;

    // Start is called before the first frame update

    public void Start()
    {
        bola = GetComponent<Rigidbody2D>();
        if  (playerTrans.localScale.x > 0) 
        {
            bola.velocity = new Vector2((+Speed), 0);
        }
        else
        {
            bola.velocity = new Vector2((-Speed), 0);
        }
 
    }

    // Update is called once per frame
    public void Update()
    {

    }

    private void OnTriggerExit2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Enemigo")
        {
            newbolaFuego = bolaFuego;
            newmatarEnemigo = Instantiate(matarEnemigo);
            Destroy(newmatarEnemigo, 1);
            Destroy(newbolaFuego);
        }


    }

    

}
