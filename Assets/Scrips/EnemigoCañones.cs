using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoCañones : MonoBehaviour
{
    public GameObject bola;
    public GameObject newBola;
    public Transform puntoDisparo;
    public GameObject sonidoBola;
    public GameObject newSonidoBola;

    private float contador;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        contador += Time.deltaTime;
        if (contador > 3)
        {
            newBola = Instantiate(bola, puntoDisparo.position, Quaternion.identity);
            Destroy(newBola, 4);
            newSonidoBola = Instantiate(sonidoBola);
            Destroy(newSonidoBola, 1);
            contador = 0;
        }
    }
}
