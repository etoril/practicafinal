using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using Unity.Profiling;
using UnityEngine;

public class EnemigoPatrulla : MonoBehaviour
{
    // Start is called before the first frame update
    private Rigidbody2D m_rig;
    public GameObject enemigo;
    public GameObject bolaFuego;
    public GameObject newbolaFuego;
    public float speed;
    public GameObject sonidoMatrEnemigo;
    public GameObject newsonidoMatrEnemigo;
    public GameObject sonidoBola;
    public GameObject newSonidoBola;
    private Animator anim;
    public bool tocado;
    
    public GameObject bola;
    public GameObject newBola;
    public Transform puntoDisparo;

    private float contador;

    void Start()
    {
        m_rig = GetComponent<Rigidbody2D>();
        anim = enemigo.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        m_rig.velocity = new Vector2(speed,m_rig.velocity.y);
        contador += Time.deltaTime;
        if (contador > 3)
        {
            newBola = Instantiate(bola, puntoDisparo.position, Quaternion.identity);
            Destroy(newBola, 4);
            newSonidoBola = Instantiate(sonidoBola);
            Destroy(newSonidoBola, 1);
            contador = 0;
        }
       
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "suelo")
        {
            speed *= -1;
            this.transform.localScale = new Vector2(this.transform.localScale.x * -1,this.transform.localScale.y);
        }

            if (collision.gameObject.tag == "BolaFuego")
            {
            newbolaFuego = bolaFuego;
            tocado = true;
            anim.SetBool("tocado", tocado);
            Destroy(enemigo,1);
            
            //            Destroy(newbolaFuego);
            newsonidoMatrEnemigo = Instantiate(sonidoMatrEnemigo);
            Destroy(newsonidoMatrEnemigo, 5);
            }


    }
}
