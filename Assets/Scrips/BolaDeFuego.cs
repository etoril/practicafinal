using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    private Rigidbody2D bola;
    public float Speed;

    // Start is called before the first frame update
    void Start()
    {
        bola = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        bola.velocity = new Vector2(+Speed,0);
    }
}
