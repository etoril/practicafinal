using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoCañon : MonoBehaviour
{
    private Rigidbody2D bola;
    private Rigidbody2D newBola;
    public float Speed;
    public Transform puntoDisparo;
    public GameObject bolaFuego;
    public GameObject newbolaFuego;
    public GameObject matarEnemigo;
    public GameObject newMatarEnemigo;


    // Start is called before the first frame update


    public void Start()
    {

        bola = GetComponent<Rigidbody2D>();
            bola.velocity = new Vector2((Speed), 0);


    }

    // Update is called once per frame
    public void Update()
    {
   
    }

    private void OnTriggerExit2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {
            newbolaFuego = bolaFuego;
            newMatarEnemigo = Instantiate(matarEnemigo);
            Destroy(newMatarEnemigo, 1);
            Destroy(newbolaFuego);
        }

    }
}
