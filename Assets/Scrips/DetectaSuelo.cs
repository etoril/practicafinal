using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectaSuelo : MonoBehaviour
{
    private PlayerController player;


    //   // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<PlayerController>();

   
    }

    // Update is called once per frame
    void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.tag == "suelo" )
        {
            player.tocasuelo = true;

        }
        
    }

    void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.tag != "suelo" )
        {
            player.tocasuelo = false;

        }

    }
}

