using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEditor;
using UnityEngine;

public class MoverPlataforma : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform from;
    public Transform to;
    public GameObject plataforma;
    public float speed;

    private Vector3 MoverHacia;

    public void Start()
    {
        MoverHacia = to.position;
    }


    public void Update()
    {
        plataforma.transform.position = Vector3.MoveTowards(plataforma.transform.position,MoverHacia,speed * Time.deltaTime);

        if (plataforma.transform.position == to.position)
        {
            MoverHacia = from.position;
            
        }


        if (plataforma.transform.position == from.position)
        {
            MoverHacia = to.position;
        
        }

    }
}
