using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuFin : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Conti;
    public GameObject Fin;
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Continuar()
    {
        Instantiate(Conti);
        SceneManager.LoadScene("Pantalla1");
    }

    public void Acabar()
    {
        Instantiate(Fin);
        SceneManager.LoadScene("FinTotal");
    }
}
