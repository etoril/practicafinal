using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CambiarAPantalla2 : MonoBehaviour
{
    // Start is called before the first frame update
    private Rigidbody2D m_rig;

    void Start()
    {
        m_rig = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            SceneManager.LoadScene("Pantalla2");
        }


    }
}
